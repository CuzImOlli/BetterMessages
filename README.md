# BetterMessages

![Image of Bettermessages](https://proxy.spigotmc.org/7c006a97dcbcc39c22f36e1e507cff8d9aa71ba0?url=http%3A%2F%2FImOlli.de%2FBetterMessages%2Fimage1.png)

Important! If errors occur after updates: Try to delete the configs in the plugin folder.

**BetterMessages** is an plugin which allows you to change messages of the server. Like the join and leave messages. All messages are individual configurable in the messages.yml config.

## Features:
    - Join | Changes the join message which appears, if an player joined the server

    - Quit | Changes the leave message which appears, if an player quit the
    server.

    - Chat | Changes the chat format.

    - Kick | Changes the kick message which appears, if an player was kicked from the server

    - Bedenter | Shows an message to all players, if an player enters an bed.

    - Bedleave | Shows an message to all players, if an player leaves an bed.  

## Config help:
    - %display_name% | Will be replaced with the display name of an player.

    - %msg% | Will be replaced with the chat message from the player. (Only for chat feature!)

    - %name% | Will be replaced with the name of an player.
    
## Commands:
    - /bettermessages [reload] | Basic Command.
    
## Permissions:
    - /bettermessages | BetterMessages.bettermessages

    Ultimate Permission:
    - bettermessages.*
