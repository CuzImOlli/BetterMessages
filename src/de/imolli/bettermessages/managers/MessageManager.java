package de.imolli.bettermessages.managers;

import de.imolli.bettermessages.BetterMessages;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;

public class MessageManager {

    private static File file;
    private static YamlConfiguration config;
    private static HashMap<String, String> messages;

    public static void init() {

        messages = new HashMap<>();
        file = new File("plugins//BetterMessages//messages.yml");

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                BetterMessages.getInstance().getLogger().log(Level.INFO, "An error occurred while creating 'messages.yml'!");
                return;
            }
        }

        config = YamlConfiguration.loadConfiguration(file);

        config.options().copyDefaults(true);
        config.options().header("Messages configuration of BetterMessages \n\n" +
                "Please note that editing the configurations while the server is running is not recommended.\n\n");

        config.addDefault("BetterMessages.prefix", "&9BetterMessages &8» ");
        config.addDefault("BetterMessages.reload", "&aReloaded all configurations.");
        config.addDefault("BetterMessages.noperm", "&cYou don't have enough permissions for that!");
        config.addDefault("BetterMessages.console.notplayer", "You are not an player!");
        config.addDefault("BetterMessages.error.basic", "&cAn error occurred!");
        config.addDefault("BetterMessages.error.cmd", "&cUse %cmd%&c!");

        //==== Features ====//

        config.addDefault("BetterMessages.features.join", "&e%display_name% &7has joined the server.");
        config.addDefault("BetterMessages.features.quit", "&e%display_name% &7has left the server.");
        config.addDefault("BetterMessages.features.chat", "&8[&e%display_name%&8] &7%msg%");
        config.addDefault("BetterMessages.features.kick", "&e%display_name% &cwas kicked from the server!");
        config.addDefault("BetterMessages.features.bedenter", "&e%display_name% &7is now sleeping.");
        config.addDefault("BetterMessages.features.bedleave", "&e%display_name% &7is no longer sleeping.");

        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void loadConfig() {

        for (String key : config.getKeys(true)) {
            String value = config.getString(key);
            value = ChatColor.translateAlternateColorCodes('&', value);

            messages.put(key, value);
        }

        BetterMessages.setPrefix(messages.getOrDefault("BetterMessages.prefix", ""));
    }

    public static String getMessage(String name) {
        return messages.getOrDefault(name, name);
    }


    public static HashMap<String, String> getMessages() {
        return messages;
    }
}
