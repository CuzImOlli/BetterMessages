package de.imolli.bettermessages;

import de.imolli.bettermessages.commands.COMMAND_bettermessages;
import de.imolli.bettermessages.features.FeatureManager;
import de.imolli.bettermessages.listeners.*;
import de.imolli.bettermessages.managers.MessageManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class BetterMessages extends JavaPlugin {

    private static Plugin plugin;
    private static String prefix;

    @Override
    public void onEnable() {
        plugin = this;

        checkConfig();
        MessageManager.init();
        MessageManager.loadConfig();
        FeatureManager.init();
        registerCommands();
        registerListeners();
    }

    public static void checkConfig() {

        plugin.getConfig().options().copyDefaults(true);
        plugin.getConfig().options().header("Configuration of BetterMessages \n\n" +
                "Please note that editing the configurations while the server is running is not recommended. \n\n");

        plugin.getConfig().addDefault("Features.join", true);
        plugin.getConfig().addDefault("Features.quit", true);
        plugin.getConfig().addDefault("Features.chat", true);
        plugin.getConfig().addDefault("Features.kick", false);
        plugin.getConfig().addDefault("Features.bedenter", false);
        plugin.getConfig().addDefault("Features.bedleave", false);
        plugin.saveConfig();

    }

    private void registerCommands() {

        Bukkit.getPluginCommand("bettermessages").setExecutor(new COMMAND_bettermessages());

    }

    private void registerListeners() {

        Bukkit.getPluginManager().registerEvents(new PlayerTabCompleteListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerQuitListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerChatListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerKickListener(), this);
        Bukkit.getPluginManager().registerEvents(new BedListener(), this);

    }

    public static void setPrefix(String newprefix) {
        prefix = newprefix;
    }

    public static String getPrefix() {
        return prefix;
    }

    public static Plugin getInstance() {
        return plugin;
    }
}
