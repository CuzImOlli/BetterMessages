package de.imolli.bettermessages.features;

public class Feature {

    private String name;
    private String message;
    private Boolean enabled;

    public Feature(String name, String message, Boolean enabled) {
        this.name = name;
        this.message = message;
        this.enabled = enabled;
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }

    public Boolean getEnabled() {
        return enabled;
    }
}
