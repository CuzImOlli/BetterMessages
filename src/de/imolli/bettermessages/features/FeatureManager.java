package de.imolli.bettermessages.features;

import de.imolli.bettermessages.BetterMessages;
import de.imolli.bettermessages.managers.MessageManager;

import java.util.HashMap;

public class FeatureManager {

    private static HashMap<String, Feature> features;

    public static void init() {

        features = new HashMap<>();

        loadConfig();

    }

    public static void loadConfig() {

        for (String key : BetterMessages.getInstance().getConfig().getKeys(true)) {
            if (key.startsWith("Features.")) {

                String name = key.replace("Features.", "").toLowerCase();
                Boolean enabled = BetterMessages.getInstance().getConfig().getBoolean(key);

                Feature feature = new Feature(name, MessageManager.getMessage("BetterMessages.features." + name), enabled);

                features.put(name, feature);

            }
        }
    }

    public static Feature getFeature(String name) {

        name = name.toLowerCase();

        if (features.containsKey(name)) {
            return features.get(name);
        }

        return null;
    }

    public static HashMap<String, Feature> getFeatures() {
        return features;
    }
}
