package de.imolli.bettermessages.listeners;

import de.imolli.bettermessages.features.Feature;
import de.imolli.bettermessages.features.FeatureManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;

public class PlayerKickListener implements Listener {

    @EventHandler
    public void onKick(PlayerKickEvent e) {

        Player p = e.getPlayer();

        Feature feature = FeatureManager.getFeature("kick");

        if (feature.getEnabled()) {

            String msg = feature.getMessage();

            msg = msg.replaceAll("%display_name%", p.getDisplayName());
            msg = msg.replaceAll("%name%", p.getName());

            Bukkit.broadcastMessage(msg);

        }
    }
}
