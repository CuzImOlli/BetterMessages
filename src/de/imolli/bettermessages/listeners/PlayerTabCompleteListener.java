package de.imolli.bettermessages.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.TabCompleteEvent;

import java.util.ArrayList;
import java.util.List;

public class PlayerTabCompleteListener implements Listener {

    @EventHandler
    public void onTabComplete(TabCompleteEvent e) {

        String buffer = e.getBuffer();

        if (buffer.equalsIgnoreCase("/bettermessages ")) {

            List<String> completions = new ArrayList<>();
            completions.add("reload");
            e.setCompletions(completions);

        }
    }
}
