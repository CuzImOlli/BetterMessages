package de.imolli.bettermessages.listeners;

import de.imolli.bettermessages.features.Feature;
import de.imolli.bettermessages.features.FeatureManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PlayerChatListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {

        Player p = e.getPlayer();

        Feature feature = FeatureManager.getFeature("chat");

        if (feature.getEnabled()) {

            String msg = feature.getMessage();

            msg = msg.replaceAll("%display_name%", p.getDisplayName());
            msg = msg.replaceAll("%name%", p.getName());
            msg = msg.replaceAll("%msg%", e.getMessage());

            e.setFormat(msg);

        }
    }
}
