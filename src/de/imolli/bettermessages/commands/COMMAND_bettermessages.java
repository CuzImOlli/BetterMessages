package de.imolli.bettermessages.commands;

import de.imolli.bettermessages.BetterMessages;
import de.imolli.bettermessages.features.FeatureManager;
import de.imolli.bettermessages.managers.MessageManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class COMMAND_bettermessages implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if (!(cs instanceof Player)) {
            cs.sendMessage(MessageManager.getMessage("BetterMessages.console.notplayer"));
            return true;
        }

        Player p = (Player) cs;

        if (!p.hasPermission("BetterMessages.bettermessages")) {
            return false;
        }

        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("reload")) {
                BetterMessages.getPlugin(BetterMessages.class).reloadConfig();
                BetterMessages.checkConfig();
                MessageManager.init();
                MessageManager.loadConfig();
                FeatureManager.init();
                p.sendMessage(BetterMessages.getPrefix() + MessageManager.getMessage("BetterMessages.reload"));
                return true;
            } else {
                p.sendMessage(BetterMessages.getPrefix() + MessageManager.getMessage("BetterMessages.error.cmd").replaceAll("%cmd%", "/BetterMessages [reload]"));
                return true;
            }
        } else {
            p.sendMessage(BetterMessages.getPrefix() + MessageManager.getMessage("BetterMessages.error.cmd").replaceAll("%cmd%", "/BetterMessages [reload]"));
            return true;
        }
    }
}
